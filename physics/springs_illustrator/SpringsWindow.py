import sys

from physics.nn.network import MutatableNetwork
from physics.springs.consts import FPS
from physics.vector import Vector
from pygame_example.GameWindow import GameWindow
import pygame
from physics.springs.springs import Springs
from physics.springs.joint import Joint
from physics.springs.constraint import Constraint
from physics.springs.integrator import NewtonIntegrator, VerletIntegrator


def initialize_springs():
    sections = 10
    length = 50
    top_row = [Joint(100 + i * length, 100) for i in range(sections)]
    bottom_row = [Joint(100 + i * length, 100 + length) for i in range(sections)]
    joints = top_row + bottom_row
    constraints = [Constraint(j1, j2, j1.distance(j2)) for j1, j2 in zip(top_row, bottom_row)] + \
                  [Constraint(j1, j2, j1.distance(j2)) for j1, j2 in zip(top_row[1:], top_row[:-1])] + \
                  [Constraint(j1, j2, j1.distance(j2)) for j1, j2 in zip(bottom_row[1:], bottom_row[:-1])] + \
                  [Constraint(j1, j2, j1.distance(j2)) for j1, j2 in zip(top_row[1:], bottom_row[:-1])] + \
                  [Constraint(j1, j2, j1.distance(j2)) for j1, j2 in zip(top_row[:-1], bottom_row[1:])]
    return Springs(constraints, joints, VerletIntegrator())


def prepare_inputs(springs):
    return [1 if constraint.is_strained else 0 for constraint in springs.constraints]


def apply_outputs(outputs, springs):
    for constraint, output in zip(springs.constraints, outputs):
        if output > 0.5:
            constraint.strain()
        else:
            constraint.release()


def train():
    best_brain = None
    inputs = 46
    mid_neurons = 30
    outputs = 46
    frames = 300
    generation_size = 2
    generations = 1

    brains = [MutatableNetwork(inputs, mid_neurons, outputs) for _ in range(generation_size)]

    def grade_brain(brain):
        springs = initialize_springs()

        for _ in range(frames):
            inputs = prepare_inputs(springs)
            outputs = brain.feed_forward(inputs)
            apply_outputs(outputs, springs)
            springs.update()
        brain.rating = springs.joints[0].x

    for _ in range(generations):
        worst_brain = brains[0]
        best_brain = brains[0]
        for brain in brains:
            brain.mutate()
            grade_brain(brain)
            if worst_brain.rating > brain.rating:
                worst_brain = brain
            if best_brain.rating < brain.rating:
                best_brain = brain
        brains.remove(worst_brain)
        brains.append(MutatableNetwork(inputs, mid_neurons, outputs))

    return best_brain


class SpringsWindow(GameWindow):
    def __init__(self):
        super(SpringsWindow, self).__init__((800, 600), fps=FPS),

        self.key_listener = [
            {
                "condition": lambda: pygame.mouse.get_pressed()[0],
                "trigger": self.left_click,
                "is_pressed": False
            },
            {
                "condition": lambda: pygame.mouse.get_pressed()[2],
                "trigger": self.right_click,
                "is_pressed": False
            },
            {
                "condition": lambda: pygame.key.get_pressed()[pygame.K_SPACE],
                "trigger": self.stop_resume,
                "is_pressed": False
            },
            {
                "condition": lambda: pygame.key.get_pressed()[pygame.K_DELETE],
                "trigger": self.delete_pressed,
                "is_pressed": False
            }
        ]


        # jointses = 10
        # joints = [Joint(100 + i * 30, 100) for i in range(jointses)]
        # constraints = [Constraint(j1, j2) for j1, j2 in zip(joints[:-1], joints[1:])]
        self.brain = train()

        self.springs = initialize_springs()


        self.control_joint = None
        self.is_left_pressed = False
        self.is_right_pressed = False
        self.is_space_pressed = False
        self.is_running = True
        self.scroll = 0
        self.mouse = Vector(pygame.mouse.get_pos())

    def handle_event(self, event):
        if event.type == pygame.QUIT:
            sys.exit()

    def update(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_RIGHT]:
            self.scroll -= 2
        if pressed[pygame.K_LEFT]:
            self.scroll += 2

        self.mouse = Vector(pygame.mouse.get_pos()) - Vector(self.scroll, 0)
        for listener in self.key_listener:
            if listener["condition"]():
                listener["is_pressed"] = True
            elif listener["is_pressed"]:
                listener["trigger"]()
                listener["is_pressed"] = False

        if pygame.mouse.get_pressed()[0]:
            if not self.is_shift():
                self.control_joint.x = self.mouse.x
                self.control_joint.y = self.mouse.y
                self.control_joint.velocity = Vector(0, 0)

        if self.is_running:
            inputs = prepare_inputs(self.springs)
            outputs = self.brain.feed_forward(inputs)
            apply_outputs(outputs, self.springs)
            self.springs.update()

    def is_shift(self):
        return pygame.key.get_pressed()[pygame.K_LSHIFT]

    def delete_pressed(self):
        if self.control_joint is not None:
            constraints_to_remove = []
            for constraint in self.springs.constraints:
                if constraint.joint1 is self.control_joint or constraint.joint2 is self.control_joint:
                    constraints_to_remove.append(constraint)
            for constraint in constraints_to_remove:
                self.springs.constraints.remove(constraint)
            self.springs.joints.remove(self.control_joint)
            self.control_joint = None

    def left_click(self):
        if self.control_joint is not None and self.is_shift():
            closest_joint = self.get_closest_joint(self.mouse)
            if self.mouse.distance(closest_joint) > closest_joint.mass * 10:
                new_joint = Joint(self.mouse.x, self.mouse.y)
                new_constraint = Constraint(new_joint, self.control_joint,
                                            length=new_joint.distance(self.control_joint))
                self.springs.joints.append(new_joint)
                self.springs.constraints.append(new_constraint)
            else:
                new_constraint = Constraint(closest_joint, self.control_joint,
                                            length=closest_joint.distance(self.control_joint))
                self.springs.constraints.append(new_constraint)

    def right_click(self):
        closest_joint = self.get_closest_joint(self.mouse)
        self.control_joint = closest_joint

    def get_closest_joint(self, location):
        closest_distance = 9999999.0
        closest_joint = None
        for joint in self.springs.joints:
            distance = joint.distance(location)
            if distance < closest_distance:
                closest_distance = distance
                closest_joint = joint
        return closest_joint

    def stop_resume(self):
        self.is_running = not self.is_running

    def render(self):
        self.display.fill((255, 255, 255))

        for constraint in self.springs.constraints:
            pygame.draw.line(self.display, (0, 0, 0), constraint.joint1 + Vector(self.scroll, 0), constraint.joint2 + Vector(self.scroll, 0), 1)
        for joint in self.springs.joints:
            color = (0, 0, 0) if joint is not self.control_joint else (0, 255, 0)
            pygame.draw.circle(self.display, color, (int(joint.x + self.scroll), int(joint.y)), int(joint.mass) * 5)
        pygame.draw.line(self.display, (0, 0, 0), (0, 500), (1000, 500), 1)

        x = self.scroll % 500
        pygame.draw.line(self.display, (0,0,0), (x, 500), (x, 600))
        pygame.draw.line(self.display, (0, 0, 0), (x + 500, 500), (x + 500, 600))

        # self.display.fill((255, 255, 255))
        #
        # for constraint in self.springs.constraints:
        #     pygame.draw.line(self.display, (0, 0, 0), constraint.joint1, constraint.joint2, 1)
        # for joint in self.springs.joints:
        #     color = (0, 0, 0) if joint is not self.control_joint else (0, 255, 0)
        #     pygame.draw.circle(self.display, color, (int(joint.x), int(joint.y)), int(joint.mass) * 5)
        # pygame.draw.line(self.display, (0, 0, 0), (0, 500), (1000, 500), 1)


SpringsWindow().main_loop()
