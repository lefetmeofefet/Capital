import numpy as np

MUTATE_CHANCE = 0.05


class MutatableNetwork(object):
    def __init__(self, inputs, mid_neurons, outputs):
        self.inputs = inputs
        self.mid_neurons = mid_neurons
        self.outputs = outputs
        self.synapses0 = []
        self.synapses1 = []
        self.randomize()
        self.rating = 0

    @staticmethod
    def sigmoid(x):
        output = 1 / (1 + np.exp(-x))
        return output

    def feed_forward(self, inputs):
        layer_1 = self.sigmoid(np.dot(inputs, self.synapses0))
        layer_2 = self.sigmoid(np.dot(layer_1, self.synapses1))
        return layer_2

    def mutate(self):
        for i in range(len(self.synapses0)):
            for j in range(len(self.synapses0[0])):
                if np.random.rand() < MUTATE_CHANCE:
                    self.synapses0[i, j] = 2 * np.random.rand() - 1
        for i in range(len(self.synapses1)):
            for j in range(len(self.synapses1[0])):
                if np.random.rand() < MUTATE_CHANCE:
                    self.synapses1[i, j] = 2 * np.random.rand() - 1

    def randomize(self):
        self.synapses0 = 2 * np.random.random((self.inputs, self.mid_neurons)) - 1
        self.synapses1 = 2 * np.random.random((self.mid_neurons, self.outputs)) - 1
