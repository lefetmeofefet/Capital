import numpy as np


class Vector(object):
    def __init__(self, *args):
        if len(args) == 1:
            self.x = args[0][0]
            self.y = args[0][1]
        else:
            self.x = args[0]
            self.y = args[1]

    def __add__(self, v):
        return Vector(self.x + v.x, self.y + v.y)

    def __sub__(self, v):
        return Vector(self.x - v.x, self.y - v.y)

    def __neg__(self):
        return Vector(-self.x, -self.y)

    def __rmul__(self, n):
        return self.__mul__(n)

    def __mul__(self, v):
        if isinstance(v, (int, float, complex)):
            return Vector(self.x * v, self.y * v)
        elif isinstance(v, Vector):
            return Vector(self.x * v.x, self.y * v.y)

    def __truediv__(self, v):
        return self._divide(v)

    def __floordiv__(self, v):
        return self._divide(v)

    def __lt__(self, v):
        return self.x * self.x + self.y * self.y < v.x * v.x + v.y * v.y

    def __le__(self, v):
        return self.x * self.x + self.y * self.y <= v.x * v.x + v.y * v.y

    def __eq__(self, v):
        return self.x == v.x and self.y == v.y

    def _divide(self, v):
        if isinstance(v, (int, float, complex)):
            return Vector(self.x / v, self.y / v)
        elif isinstance(v, Vector):
            return Vector(self.x / v.x, self.y / v.y)

    def add(self, v):
        self.x += v.x
        self.y += v.y

    def reversed(self):
        return Vector(-self.x, -self.y)

    def direction(self, v=None):
        if v:
            return (v - self).direction()
        else:
            return np.arctan2(self.y, self.x)

    def rotate(self, direction):
        current_direction = self.direction()
        new_angle = (current_direction + direction) % (np.pi * 2)
        self.set_direction(new_angle)

    def set_direction(self, direction):
        magnitude = self.magnitude()
        self.x = magnitude * np.cos(direction)
        self.y = magnitude * np.sin(direction)

    def magnitude(self):
        return np.sqrt(self.x * self.x + self.y * self.y)

    def set_magnitude(self, magnitude):
        direction = self.direction()
        self.x = magnitude * np.cos(direction)
        self.y = magnitude * np.sin(direction)

    def normalize(self):
        self.set_magnitude(1.0)

    def get_position(self):
        return self.x, self.y

    def distance(self, v):
        return (self - v).magnitude()

    def __getitem__(self, item):
        if item == 0:
            return self.x
        elif item == 1:
            return self.y

    def __len__(self):
        return 2

    def __str__(self):
        return str(self.x) + ", " + str(self.y)
