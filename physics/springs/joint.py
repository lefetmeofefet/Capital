from physics.vector import Vector


class Joint(Vector):
    def __init__(self, x, y, mass=1):
        super().__init__(x, y)
        self.velocity = Vector(0, 0)
        self.acceleration = Vector(0, 0)
        self.mass = mass
        self.last_acceleration = Vector(0, 0)

    def apply_force(self, force):
        self.acceleration += force / self.mass

    def __str__(self):
        return "Location: {}, Velocity: {}, Acceleration: {}, mass: {}".format(
            str(self.x) + ", " + str(self.y), self.velocity, self.acceleration, self.mass
        )

