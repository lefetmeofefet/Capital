from physics.vector import Vector


class NewtonIntegrator(object):
    def integrate(self, body):
        body.velocity.add(body.acceleration)
        body.add(body.velocity)
        body.acceleration = Vector(0, 0)


class VerletIntegrator(object):
    def integrate(self, joint, timestep):
        # Update velocities
        dv = (joint.acceleration + joint.last_acceleration) * timestep * 0.25
        joint.velocity.add(dv)
        # Update locations
        dx = joint.velocity * timestep + joint.acceleration * timestep * timestep * 1.0
        joint.add(dx)
        # Initialize accelerations
        joint.last_acceleration = joint.acceleration
        joint.acceleration = Vector(0, 0)


class JavaVerletIntegrator(object):
    def integrate(self, joint, timestep):
        # Update locations
        dx = joint.velocity * timestep + joint.acceleration * timestep * timestep
        joint.add(dx)
        # Update velocities
        dv = timestep * joint.acceleration / 2.0
        joint.velocity.add(dv)
