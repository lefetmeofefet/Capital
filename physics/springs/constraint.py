from physics.springs.consts import STIFFNESS, LENGTH


class Constraint(object):
    def __init__(self, joint1, joint2, length=LENGTH, stiffness=STIFFNESS):
        self.joint1 = joint1
        self.joint2 = joint2
        self.length = length
        self.stiffness = stiffness
        self.is_strained = False

    def calculate_force(self):
        displacement = self.length - self.joint1.distance(self.joint2)
        force_magnitude = displacement * self.stiffness

        # max_magnitude = 0.001
        # if force_magnitude > max_magnitude:
        #     force_magnitude = max_magnitude

        direction = self.joint1 - self.joint2

        direction.set_magnitude(force_magnitude)
        return direction

    def strain(self):
        if not self.is_strained:
            self.length -= 20
        self.is_strained = True

    def release(self):
        if self.is_strained:
            self.length += 20
        self.is_strained = False
