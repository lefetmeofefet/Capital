from physics.vector import Vector
from physics.springs.consts import GRAVITY_FORCE, INEFFICIENCY_MULTIPLIER, TIMESTEP


class Springs(object):
    def __init__(self, constraints, joints, integrator):
        self.constraints = constraints
        self.joints = joints
        self.integrator = integrator

    def update(self):
        for constraint in self.constraints:
            force = constraint.calculate_force()
            constraint.joint1.apply_force(force)
            constraint.joint2.apply_force(-force)

        for joint in self.joints:
            # Gravity
            joint.apply_force(Vector(0, GRAVITY_FORCE))
            # Inefficiency
            joint.apply_force(-joint.velocity * INEFFICIENCY_MULTIPLIER)
            # Ground
            self.apply_ground_friction(joint)

            self.integrator.integrate(joint, TIMESTEP)

    def apply_ground_friction(self, joint):
        if joint.y > 500 - joint.mass * 5:
            joint.y = 500 - joint.mass * 5
            joint.velocity.y = -joint.velocity.y * 0.5
            joint.velocity.x *= 0.5
