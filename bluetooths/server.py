import bluetooth


# uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.bind(("", bluetooth.PORT_ANY)) # "a0:88:69:9f:cd:14"
s.listen(1)


bluetooth.advertise_service(s, "helloService",
                            # service_id=uuid,
                            service_classes=[bluetooth.SERIAL_PORT_CLASS],
                            profiles=[bluetooth.SERIAL_PORT_PROFILE])
try:
    client, address = s.accept()
    print("Accepted address: " + str(address))
    while True:
        data = client.recv(1024)
        if data:
            print(data)
            client.send(data) # Echo back to client
except:
    print("Closing socket")
    client.close()
    s.close()
