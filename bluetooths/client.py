import bluetooth

serverMACAddress = 'a0:88:69:9f:cd:14'
port = 3
s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.connect((serverMACAddress, port))
while 1:
    text = input()  # Note change to the old (Python 2) raw_input
    if text == "quit":
        break
    s.send(text)
s.close()
