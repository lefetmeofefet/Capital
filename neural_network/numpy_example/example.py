import numpy as np




def sigmoid(x):
    output = 1 / (1 + np.exp(-x))
    return output


def feed_forward(inputs, synapses0, synapses1):
    layer_1 = sigmoid(np.dot(inputs, synapses0))
    layer_2 = sigmoid(np.dot(layer_1, synapses1))
    return layer_2


# synapses0 = 2 * np.random.random((2, 3)) - 1
# synapses1 = 2 * np.random.random((3, 1)) - 1
synapses0 = np.ones((2, 3))
synapses1 = np.ones((3, 1))
synapses0[0, 1] =2
inputs = [1, 0]


print(synapses0)
print(synapses1)
print(feed_forward(inputs, synapses0, synapses1))


def doit():
    # compute sigmoid nonlinearity


    # convert output of sigmoid function to its derivative
    def sigmoid_output_to_derivative(output):
        return output * (1 - output)

    X = np.array([[0, 0, 1],
                  [0, 1, 1],
                  [1, 0, 1],
                  [1, 1, 1]])

    y = np.array([[0],
                  [1],
                  [1],
                  [0]])

    alpha = 10
    np.random.seed(1)

    # randomly initialize our weights with mean 0
    synapse_0 = 2 * np.random.random((49, 60)) - 1
    synapse_1 = 2 * np.random.random((60, 10)) - 1

    for j in range(60000):
        # Feed forward through layers 0, 1, and 2
        layer_0 = X
        layer_1 = sigmoid(np.dot(layer_0, synapse_0))
        layer_2 = sigmoid(np.dot(layer_1, synapse_1))

        # how much did we miss the target value?
        layer_2_error = layer_2 - y

        if (j % 10) == 0:
            print("Error after " + str(j) + " iterations:" + str(np.mean(np.abs(layer_2_error))))

        # in what direction is the target value?
        # were we really sure? if so, don't change too much.
        layer_2_delta = layer_2_error * sigmoid_output_to_derivative(layer_2)

        # how much did each l1 value contribute to the l2 error (according to the weights)?
        layer_1_error = layer_2_delta.dot(synapse_1.T)

        # in what direction is the target l1?
        # were we really sure? if so, don't change too much.
        layer_1_delta = layer_1_error * sigmoid_output_to_derivative(layer_1)

        synapse_1 -= alpha * (layer_1.T.dot(layer_2_delta))
        synapse_0 -= alpha * (layer_0.T.dot(layer_1_delta))

