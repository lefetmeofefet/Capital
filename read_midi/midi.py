import pygame
import pygame.midi
from read_midi.Keyboard import Keyboard


# display a list of MIDI devices connected to the computer
def print_device_info():
    for i in range(pygame.midi.get_count()):
        r = pygame.midi.get_device_info(i)
        (interface, name, is_input, output, opened) = r
        in_out = ""
        if is_input:
            in_out = "(input)"
        if output:
            in_out = "(output)"
        print("%2i: interface: %s, name: %s, opened: %s %s" % (i, interface, name, opened, in_out))


def main():
    pygame.init()
    pygame.fastevent.init()
    event_get = pygame.fastevent.get
    event_post = pygame.fastevent.post
    pygame.midi.init()
    print("Available MIDI devices:")
    print_device_info()
    # Change this to override use of default input device

    device_id = pygame.midi.get_default_input_id()
    keyboard = Keyboard(device_id=device_id)
    print("Using input_id: %s" % keyboard.device_id)

    midi_input = pygame.midi.Input(keyboard.device_id)

    while True:
        events = event_get()
        for e in events:
            if e.type in [pygame.midi.MIDIIN]:
                # print("Timestamp: " + str(e.timestamp) + "ms, Channel: " + str(e.data1) + ", Value: " + str(e.data2) +
                #       ", Status: " + str(e.status))
                PRESS = 144
                RELEASE = 128

                if e.status != 176:
                    if e.status == PRESS:
                        keyboard.key_pressed(e.data1)
                    elif e.status == RELEASE:
                        keyboard.key_released(e.data1)

        # if there are new data from the MIDI controller
        if midi_input.poll():
            midi_events = midi_input.read(10)
            midi_evs = pygame.midi.midis2events(midi_events, midi_input.device_id)
            for m_e in midi_evs:
                event_post(m_e)


if __name__ == '__main__':
    main()
