from win32com import client

from read_midi.Configurations import NORMAL as KEY_TO_LETTER


class Keyboard(object):
    def __init__(self, device_id):
        self.device_id = device_id
        self.key_eater = client.Dispatch("WScript.Shell")

    def key_pressed(self, key):
        try:
            key_to_send = KEY_TO_LETTER[key]
            self.key_eater.SendKeys(key_to_send)
        except:
            pass

    def key_released(self, key):
        pass
