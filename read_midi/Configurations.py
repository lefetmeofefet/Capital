
SI = 47
DO1 = 48
DO2 = 60
DO3 = 72


NORMAL = {
    DO1 + 9: "E",
    DO1 + 10: "c",
    DO2 + 0: "l",
    DO2 + 2: "i",
    DO2 + 3: "p",
    DO2 + 5: "s",
    DO2 + 7: "e",
    DO2 + 9: "3",
    DO2 + 10: "7",
    DO3 + 0: "3",
    DO3 + 2: "7",
    DO3 + 3: "0",
    DO3 + 10: "\n",

}


# NORMAL = {
#     # 48: "tab",
#     SI + 0: " ",
#
#     #############
#
#     SI + 1: "a",
#     SI + 2: "q",
#     SI + 3: "z",
#
#     SI + 4: "w",
#     SI + 5: "s",
#     SI + 6: "x",
#
#     SI + 7: "e",
#     SI + 8: "d",
#     SI + 9: "c",
#
#     SI + 10: "f",
#     SI + 11: "r",
#     SI + 12: "v",
#
#     #############
#
#     SI + 13: "g",
#     SI + 14: "t",
#     SI + 15: "b",
#
#     SI + 16: "y",
#     SI + 17: "h",
#     SI + 18: "n",
#
#     SI + 19: "u",
#     SI + 20: "j",
#     SI + 21: "m",
#
#     SI + 22: "k",
#     SI + 23: "i",
#     SI + 24: ",",
#
#     #############
#
#     SI + 25: "l",
#     SI + 26: "o",
#     SI + 27: ".",
#
#     SI + 28: "p",
#     SI + 29: ";",
#     SI + 30: "/",
#
#     SI + 31: "[",
#     SI + 32: "'",
#
#     ############# Utility
#
#     SI + 33: "\n"
# }
#
