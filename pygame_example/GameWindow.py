import pygame


class GameWindow(object):
    def __init__(self, size, full_screen=0, fps=60):
        pygame.init()
        self.fps = fps
        self.display = pygame.display.set_mode(size, full_screen)
        self.clock = pygame.time.Clock()

    def main_loop(self):
        while True:
            self._handle_events()
            self.update()
            self.render()
            pygame.display.flip()
            self.clock.tick(self.fps)

    def _handle_events(self):
        for event in pygame.event.get():
            self.handle_event(event)

    def handle_event(self, event):
        raise NotImplementedError("handle_event method of class GameWindow is not implemented")

    def update(self):
        raise NotImplementedError("update method of class GameWindow is not implemented")

    def render(self):
        raise NotImplementedError("render method of class GameWindow is not implemented")
