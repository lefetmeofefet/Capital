import sys
from pygame_example.GameWindow import GameWindow
import pygame


class ExampleWindow(GameWindow):
    def __init__(self):
        super(ExampleWindow, self).__init__((800, 600))
        self.font = pygame.font.SysFont('mono', 20, bold=True)
        self.block = (100, 100)

    def handle_event(self, event):
        if event.type == pygame.QUIT:
            sys.exit()

    def update(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_UP]:
            self.block = (self.block[0], self.block[1] - 4)
        if pressed[pygame.K_DOWN]:
            self.block = (self.block[0], self.block[1] + 4)
        if pressed[pygame.K_RIGHT]:
            self.block = (self.block[0] + 4, self.block[1])
        if pressed[pygame.K_LEFT]:
            self.block = (self.block[0] - 4, self.block[1])

    def render(self):
        self.display.fill((0, 0, 0))
        pygame.draw.rect(self.display, (255, 0, 0), pygame.Rect(self.block[0], self.block[1], 50, 50))

        text = "FPS: " + str(self.clock.get_fps())
        surface = self.font.render(text, True, (0, 255, 0))
        self.display.blit(surface, (0, 0))

ExampleWindow().main_loop()
