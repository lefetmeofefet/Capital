import time
import logging
from game_bot.Window import Window
from game_bot import BotState
from game_bot.bot_routines.move_to_tp import MoveToTP
from game_bot.bot_routines.create_game import CreateGame
from game_bot.bot_routines.player_specific.start import Start
from game_bot.bot_routines.player_specific.finish import Finish
from game_bot.bot_routines.exit_game import ExitGame
from game_bot.bot_routines.travel_autotele import TravelAutotele
from game_bot.bot_routines.kill_countess import KillCountess
from game_bot.bot_routines.collect_runes import CollectRunes
from game_bot.bot_routines.parallel_watcher import ParallelWatcher

window = Window("Diablo II")
exit_game = ExitGame(window)
parallel_watcher = ParallelWatcher(window)
ROUTINES = [
    CreateGame(window),
    Start(window),
    MoveToTP(window),
    TravelAutotele(window),
    KillCountess(window),
    CollectRunes(window),
    Finish(window),
    exit_game
]


def main():
    time.sleep(2)
    logging.basicConfig(filename='log/bot_log.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
    logging.info("###### STARTING HAMMERBEEF BOT ######")
    start_bot()


def start_bot():
    parallel_watcher.execute_routine()
    rounds = 1
    while True:
        try:
            logging.info("|-- Starting Round " + str(rounds))
            start_time = time.clock()
            for routine in ROUTINES:
                logging.info("|--|-- Starting Routine " + str(routine.__class__.__name__))
                routine.execute_routine()
                logging.info("|--|-- Finished Routine")
            logging.info("|-- Finished Round in " + str(time.clock() - start_time) + " Seconds, overall " + str(BotState.overall_runes_collected) + " runes")
        except Exception:
            logging.exception("Error: ")
            logging.info("|-- Attempting to execute exit_game Routine")
            exit_game.execute_routine()
        finally:
            rounds += 1


if __name__ == '__main__':
    main()
