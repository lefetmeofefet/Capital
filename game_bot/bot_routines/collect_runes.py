from game_bot import ImageWindow
import logging
import time
import datetime
import win32con
from game_bot import BotState

RUNE_COLOR = (164, 32, 252)
IMAGES_DIR = "C://Everything/Diablo_bot_images/"


class CollectRunes(object):
    def __init__(self, window):
        self.window = window

    def take_pic(self):
        self.window.key_down(win32con.VK_MENU)
        time.sleep(0.2)
        ImageWindow.save_screen_shot("Diablo II", IMAGES_DIR + str(datetime.datetime.now()).replace(":", "-")[0:-7] + ".bmp",
                                     (self.window.get_window_size()[0], self.window.get_window_size()[1]))
        self.window.key_down(win32con.VK_MENU)

    def execute_routine(self):
        self.take_pic()
        while True:
            self.window.key_press(win32con.VK_TAB)
            self.window.key_down(win32con.VK_MENU)
            time.sleep(0.3)

            rune_location = self.get_rune_location()
            if rune_location is not None:
                self.window.mouse_click((rune_location[0] - self.window.frame_width + 5,
                                         rune_location[1] - self.window.title_bar_height - self.window.frame_width + 4),
                                        move_mouse=True)
                BotState.runes_in_stash += 1
                BotState.overall_runes_collected += 1
                logging.info("|--|--|-- Collected rune!")
                time.sleep(1.5)
            else:
                if self.are_there_runes() is None:
                    self.window.key_up(win32con.VK_MENU)
                    self.window.key_press(win32con.VK_TAB)
                    time.sleep(0.3)
                    break

            self.window.key_up(win32con.VK_MENU)
            self.window.key_press(win32con.VK_TAB)
            time.sleep(0.3)

    def are_there_runes(self):
        return ImageWindow.scan_screen_for_color([RUNE_COLOR], (300, 300), (240, 160))

    def get_rune_location(self):
        return ImageWindow.scan_screen_for_color([RUNE_COLOR],
                                                 (self.window.get_window_size()[0], 580))

if __name__ == '__main__':
    time.sleep(3)
    from game_bot.Window import Window
    window = Window("Diablo II")
    CollectRunes(window).execute_routine()
