from PIL import Image
import win32con
import time
import threading
from game_bot import ImageWindow

DEAD_COUNTESS_COLORS = [
    (160, 252, 136),
    (88, 144, 76),
    (56, 92, 36)
]


class KillCountess(object):
    def __init__(self, window):
        self.window = window
        self.keep_attacking = False

    def execute_routine(self):
        self.window.key_press(win32con.VK_F3, delay=0.1)

        self.keep_attacking = True
        threading.Thread(target=self.attack).start()

        self.wait_for_countess_death()
        self.keep_attacking = False
        time.sleep(0.5)

    def attack(self):
        self.window.move_mouse((405, 7))
        time.sleep(0.1)
        self.window.mouse_down((405, 7))
        while self.keep_attacking:
            self.window.mouse_down((405, 7))
            self.window.window.SendMessage(win32con.WM_KEYDOWN, win32con.VK_SHIFT, win32con.VK_SHIFT)
            time.sleep(1 / 100)
        self.window.mouse_up((405, 7))
        self.window.window.SendMessage(win32con.WM_KEYUP, win32con.VK_SHIFT, win32con.VK_SHIFT)

    def wait_for_countess_death(self):
        time.sleep(10)


if __name__ == '__main__':
    time.sleep(3)
    from game_bot.Window import Window
    window = Window("Diablo II")
    KillCountess(window).execute_routine()
