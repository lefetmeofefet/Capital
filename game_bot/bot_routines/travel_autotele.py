import logging
import win32con
import time

TOWER_LEVELS = [
    {
        "identifiers": [
            {
                "location": (705, 48),
                "color": (148, 128, 100)
            },
            {
                "location": (740, 46),
                "color": (148, 128, 100)
            }
        ],
        "name": "tower_floor"
    },
    {
        "identifiers": [
            {
                "location": (780, 47),
                "color": (148, 128, 100)
            },
            {
                "location": (780, 53),
                "color": (148, 128, 100)
            }
        ],
        "name": "tower_level_1"
    },
    {
        "identifiers": [
            {
                "location": (777, 53),
                "color": (148, 128, 100)
            }
        ],
        "name": "tower_level_2"
    },
    {
        "identifiers": [
            {
                "location": (776, 46),
                "color": (148, 128, 100)
            }
        ],
        "name": "tower_level_3"
    },
    {
        "identifiers": [
            {
                "location": (775, 51),
                "color": (148, 128, 100)
            },
            {
                "location": (779, 53),
                "color": (148, 128, 100)
            }
        ],
        "name": "tower_level_4"
    },
    {
        "identifiers": [
            {
                "location": (777, 52),
                "color": (148, 128, 100)
            }
        ],
        "name": "tower_level_5"
    }
]


class TravelAutotele(object):
    def __init__(self, window):
        self.window = window

    def execute_routine(self):
        self.autotele()
        for level in TOWER_LEVELS:
            autoteleing_seconds = 0
            stuck_count = 0
            while True:
                if self.is_level_achieved(level["identifiers"]):
                    time.sleep(0.4)
                    logging.info("|--|--|-- Got to " + level["name"])
                    self.autotele()
                    break
                time.sleep(0.2)
                autoteleing_seconds += 0.2
                if autoteleing_seconds > 15:
                    stuck_count += 1
                    autoteleing_seconds = 0
                    logging.warning("|--|--|-- Got stuck: more than 15 seconds passed. Autoteleing again")
                    if stuck_count > 4:
                        logging.warning("|--|--|-- Been stuck " + str(stuck_count) + " times, restarting")
                        raise Exception("Stuck in autotele too many times")
                    else:
                        self.autotele()

        # Wait till getting to countess
        time.sleep(3)

    def autotele(self):
        self.window.key_press(win32con.VK_NUMPAD1, delay=0.5)

    def is_level_achieved(self, tower_level):
        for identifier in tower_level:
            real_color = self.window.get_pixel(identifier["location"])
            if identifier["color"] != real_color:
                return False
        return True
