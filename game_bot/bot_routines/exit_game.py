import win32con
import time
from game_bot import BotState

NEXT_GAME_KEY = win32con.VK_DIVIDE


class ExitGame(object):
    def __init__(self, window):
        self.window = window

    def execute_routine(self):
        self.window.key_down(NEXT_GAME_KEY)
        self.window.key_up(NEXT_GAME_KEY)
        self.wait_for_loading()
        BotState.is_in_game = False
        time.sleep(2)

    def wait_for_loading(self):
        black_points = [
            (55, 97),
            (355, 101),
            (336, 417),
            (48, 415),
            (371, 256)
        ]

        while True:
            for position in black_points:
                pix = self.window.get_pixel(position)
                if pix != (4, 4, 4):
                    time.sleep(0.1)
                    return
            time.sleep(0.1)
