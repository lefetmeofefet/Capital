import time
import win32con
import random
from game_bot import BotState

CREATE_BUTTON_LOCATION = (593, 459)


class CreateGame(object):
    def __init__(self, window):
        self.window = window
        name_length = random.randrange(7, 15)
        self.last_name = self.generate_base_name(name_length)
        self.runs_with_same_name = 0

    def execute_routine(self):
        self.window.mouse_click(CREATE_BUTTON_LOCATION)
        time.sleep(0.5)

        new_name = self.generate_name()
        self.window.write_string(new_name)

        time.sleep(0.5)

        self.window.key_press(win32con.VK_RETURN)
        time.sleep(1)
        self.wait_for_loading()
        time.sleep(0.2)
        BotState.is_in_game = True
        time.sleep(0.1)

    def generate_name(self):
        if random.randrange(5, 20) > self.runs_with_same_name:
            self.runs_with_same_name += 1
        else:
            name_length = random.randrange(7, 15)
            name = self.generate_base_name(name_length)
            self.last_name = name
            self.runs_with_same_name = 1
        return self.last_name + str(self.runs_with_same_name)

    def generate_base_name(self, name_length):
        return "".join(["a" if i%3 == 0 else "s" if i%3 == 1 else "d" for i in range(name_length)])

    def wait_for_loading(self):
        black_points = [
            (200, 100),
            (200, 500),
            (550, 500),
            (550, 100)
        ]

        while True:
            for position in black_points:
                pix = self.window.get_pixel(position)
                if pix != (0, 0, 0):
                    time.sleep(0.1)
                    return
            time.sleep(0.1)
