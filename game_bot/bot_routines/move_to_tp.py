import time
import logging
import win32con

ARBITRARY_UNIQUE_PIXEL = (765, 182)
TP_ROUTES = [
    # Most common bottom tp, with tent
    {
        "color": [(40, 40, 32), (56, 52, 44), (64, 60, 52)],
        "segments": [
            {"click": (787, 342), "wait": 1},
            {"click": (729, 277), "wait": 1},
            # Too close to Kashya
            #{"click": (730, 393), "wait": 2},
            #{"click": (785, 224), "wait": 2},
            {"click": (112, 280), "wait": 0.5}
        ]
    },
    # Other bottom tp
    {
        "color": [(48, 40, 20),
                  (48, 40, 32),
                  (48, 60, 64),
                  (32, 52, 24),
                  (28, 32, 16),
                  (28, 20, 16),
                  (36, 40, 20),
                  (56, 56, 56),
                  (36, 32, 24),
                  (24, 36, 12)],
        "segments": [
            {"click": (730, 156), "wait": 1},
            {"click": (787, 387), "wait": 1},
            {"click": (112, 280), "wait": 0.5}
        ]
    },
    # Most common top tp
    {
        "color": [(32, 24, 20)],
        "segments": [
            {"click": (504, 169), "wait": 1},
            {"click": (533, 59), "wait": 1.2},
            {"click": (112, 280), "wait": 0.5}
        ]
    },
    # Other top tp, with wooden fence
    {
        "color": [(20, 36, 24)],
        "segments": [
            {"click": (504, 169), "wait": 1},
            {"click": (454, 14), "wait": 1.3},
            {"click": (112, 280), "wait": 0.5}
        ]
    }
]
ROGUE_ENCAMPMENT_ID_PIXEL_LOCATION = (645, 50)
ROGUE_ENCAMPMENT_ID_PIXEL = (148, 128, 100)


class MoveToTP(object):
    def __init__(self, window):
        self.window = window

    def execute_routine(self):
        while True:
            arbitrary_pixel = self.window.get_pixel(ARBITRARY_UNIQUE_PIXEL)
            for route in TP_ROUTES:
                if arbitrary_pixel in route["color"]:
                    self._execute_tp_route(route)
                    time.sleep(1)
                    self.validate_black_marsh()
                    return
            logging.warning("|--|--|-- TP Route not found by any pixel. Pixel: " + str(arbitrary_pixel))
            raise Exception("Couldn't find TP route")

    def validate_black_marsh(self):
        if self.window.get_pixel(ROGUE_ENCAMPMENT_ID_PIXEL_LOCATION) == ROGUE_ENCAMPMENT_ID_PIXEL:
            logging.warning("|--|--|-- TP Route found, but black marsh not reached. restarting")
            raise Exception("TP Route found, but black marsh not reached. restarting")

    def _execute_tp_route(self, route):
        for segment in route["segments"]:
            click_location = segment["click"]
            waiting_time = segment["wait"]
            self.window.mouse_click(click_location, move_mouse=True)
            time.sleep(waiting_time)


if __name__ == '__main__':
    time.sleep(3)
    from game_bot.Window import Window
    window = Window("Diablo II")
    MoveToTP(window).execute_routine()
