import logging
import win32con
import time
import threading
from game_bot import BotState
from game_bot import ImageWindow
import datetime

LIFE_PIXEL = (62, 549)
LIFE_COLOR = (92, 0, 0)
POTIONS = [
    (437, 577),
    (469, 578),
    (500, 579),
    (529, 579)
]
MESSAGE_COLOR = (24, 252, 0)
MESSAGE_X = 17
MESSAGE_START_Y = 85
MESSAGE_END_Y = 200

IMAGES_DIR = "C://Everything/Diablo_bot_images/"


class ParallelWatcher(object):
    def __init__(self, window):
        self.window = window

    def execute_routine(self):
        threading.Thread(target=self.life_watcher).start()
        threading.Thread(target=self.message_watcher).start()

    def life_watcher(self):
        while True:
            if BotState.is_in_game:
                self.check_life()
            time.sleep(1)

    def check_life(self):
        if self.window.get_pixel(LIFE_PIXEL) != LIFE_COLOR:
            self.drink_potion()

    def drink_potion(self):
        potion_value = int(BotState.potions_drank / 4)
        if potion_value == 0:
            self.window.mouse_click(POTIONS[0], False, True)
        elif potion_value == 1:
            self.window.mouse_click(POTIONS[1], False, True)
        elif potion_value == 2:
            self.window.mouse_click(POTIONS[2], False, True)
        elif potion_value == 3:
            self.window.mouse_click(POTIONS[3], False, True)
        logging.info("|--|--|-- Life is low, drinking potion")
        BotState.potions_drank += 1

    def message_watcher(self):
        while True:
            if BotState.is_in_game:
                self.check_messages()
            time.sleep(1)

    def check_messages(self):
        if self.is_there_a_message():
            time.sleep(10)
            self.take_pic()
            self.reply()

    def take_pic(self):
        ImageWindow.save_screen_shot("Diablo II", IMAGES_DIR + str(datetime.datetime.now()).replace(":", "-")[0:-7] + "MESSAGE" + ".bmp",
                                     (self.window.get_window_size()[0], self.window.get_window_size()[1]))

    def reply(self):
        # Just exit game
        self.window.close_window()
        import sys
        sys.exit(0)

        # Reply, doesnt work
        self.window.key_press(win32con.VK_RETURN)
        time.sleep(0.05)
        self.window.write_string("/r Hahaha")
        time.sleep(0.15)
        self.window.mouse_click((0, 0), True)
        time.sleep(0.15)
        self.window.key_press(win32con.VK_RETURN, delay=1)
        #self.window.window.SendMessage(win32con.WM_KEYDOWN, win32con.VK_RETURN, 0x1000000)

    def is_there_a_message(self):
        for y in range(MESSAGE_START_Y, MESSAGE_END_Y, 3):
            if self.window.get_pixel((MESSAGE_X, y)) == MESSAGE_COLOR:
                return True
        return False

if __name__ == '__main__':
    time.sleep(3)
    from game_bot.Window import Window
    window = Window("Diablo II")
    ParallelWatcher(window).execute_routine()
