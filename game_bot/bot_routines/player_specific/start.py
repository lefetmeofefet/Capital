import time
import win32con


class Start(object):
    def __init__(self, window):
        self.window = window

    def execute_routine(self):
        self.window.key_press(self.window.get_vk_code("l"))
        self.window.key_press(win32con.VK_F5)
        time.sleep(0.1)
        self.window.mouse_click((0, 0), True, True)
        time.sleep(0.5)
        self.window.key_press(win32con.VK_F2)


if __name__ == '__main__':
    time.sleep(3)
    from game_bot.Window import Window
    window = Window("Diablo II")
    Start(window).execute_routine()
