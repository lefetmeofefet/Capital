import win32con
import time
from game_bot import ImageWindow

PATH_TO_AKARA = [
    (712, 363),
    (712, 403),
    (712, 303),
    (650, 403)
]
#AKARA_COLOR = (112, 32, 104)
AKARA_COLOR = (56, 20, 68)
POTION_LOCATION = (372, 197)


class BuyPotions(object):
    def __init__(self, window):
        self.window = window

    def execute_routine(self):
        for location in PATH_TO_AKARA:
            self.window.mouse_click(location, move_mouse=True)
            time.sleep(1)

        akara_location = self.get_akara_location()
        if akara_location is not None:
            self.window.mouse_click((akara_location[0] - self.window.frame_width + 5,
                                     akara_location[1] - self.window.title_bar_height - self.window.frame_width + 2),
                                    move_mouse=True)
            time.sleep(1)

        self.window.key_press(win32con.VK_DOWN)
        time.sleep(0.1)
        self.window.key_press(win32con.VK_RETURN)
        time.sleep(0.5)
        self.window.window.SendMessage(win32con.WM_KEYDOWN, win32con.VK_SHIFT, win32con.VK_SHIFT)
        self.window.mouse_click(POTION_LOCATION, move_mouse=True, right_button=True)

    def get_akara_location(self):
        return ImageWindow.scan_screen_for_color([AKARA_COLOR],
                                                 (self.window.get_window_size()[0], self.window.get_window_size()[1]))

if __name__ == '__main__':
    time.sleep(3)
    from game_bot.Window import Window

    window = Window("Diablo II")
    BuyPotions(window).execute_routine()
