from game_bot.ImageWindow import ImageWindow
import win32gui
import win32ui
import win32con
import win32api
import time
import threading


class Window(object):
    RESOLUTION = (800, 600)

    def __init__(self, window_name):
        self.window_name = window_name
        self.window = win32ui.FindWindow(None, window_name)
        self.hwnd = self.window.GetSafeHwnd()
        self.dc = self.window.GetWindowDC()
        self.frame_width = win32api.GetSystemMetrics(win32con.SM_CYEDGE) + win32api.GetSystemMetrics(
            win32con.SM_CYBORDER)
        self.title_bar_height = win32api.GetSystemMetrics(win32con.SM_CYCAPTION)
        self.get_pixel_lock = threading.Lock()
        self.get_dc_lock = threading.Lock()

    def move_mouse(self, position):
        win32gui.PostMessage(self.hwnd,
                             win32con.WM_MOUSEMOVE,
                             win32con.WM_MOUSEMOVE,
                             self._make_long(position[0], position[1]))

    def mouse_click(self, position, move_mouse=False, right_button=False):
        if move_mouse:
            self.move_mouse(position)
            time.sleep(0.1)
        self.mouse_down(position, right_button)
        self.mouse_up(position, right_button)

    @staticmethod
    def _make_long(low, high):
        return low | (high << 16)

    def mouse_down(self, position, right_button=False):
        win32gui.PostMessage(self.hwnd,
                             win32con.WM_RBUTTONDOWN if right_button else win32con.WM_LBUTTONDOWN,
                             win32con.MK_RBUTTON if right_button else win32con.MK_LBUTTON,
                             self._make_long(position[0], position[1]))

    def mouse_up(self, position, right_button=False):
        win32gui.PostMessage(self.hwnd,
                             win32con.WM_RBUTTONUP if right_button else win32con.WM_LBUTTONUP,
                             win32con.MK_RBUTTON if right_button else win32con.MK_LBUTTON,
                             self._make_long(position[0], position[1]))

    def mouse_scroll(self, up):
        win32gui.PostMessage(self.hwnd,
                             win32con.WM_MOUSEWHEEL,
                             0x00780000 if up else 0xff880000)

    def close_window(self):
        win32gui.PostMessage(self.hwnd,
                             win32con.WM_CLOSE,
                             0, 0)

    def key_press(self, key, delay=None):
        self.key_down(key)
        if delay:
            time.sleep(delay)
        self.key_up(key)

    def key_down(self, key):
        self.window.SendMessage(win32con.WM_KEYDOWN, key)

    def key_up(self, key):
        self.window.SendMessage(win32con.WM_KEYUP, key)

    def write_string(self, string):
        for char in string:
            self.window.SendMessage(win32con.WM_CHAR, ord(char), 0)

    def get_vk_code(self, char):
        if type(char) != str:
            return 0x30 + char
        else:
            return ord(char.lower()) - (97 - 65)

    def get_absolute_window_rect(self):
        return win32gui.GetWindowRect(self.hwnd)

    def get_window_size(self):
        self.get_dc_lock.acquire()
        result = self.dc.GetClipBox()[2], self.dc.GetClipBox()[3]
        self.get_dc_lock.release()
        return result

    def get_resolution(self):
        return self.RESOLUTION

    def get_pixel(self, position):
        x = position[0] + self.frame_width
        y = position[1] + self.frame_width + self.title_bar_height
        self.get_pixel_lock.acquire()
        color = self.dc.GetPixel(x, y)
        self.get_pixel_lock.release()
        red = color & 255
        green = (color >> 8) & 255
        blue = (color >> 16) & 255
        return red, green, blue

    def launch_image(self):
        ImageWindow(self, "file.bmp")


if __name__ == '__main__':
    Window("Diablo II").launch_image()
