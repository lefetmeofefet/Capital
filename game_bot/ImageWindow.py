# coding = utf-8

import win32api
import tkinter as tk
from PIL import ImageTk, Image
import win32gui
import win32ui
import win32con


class ImageWindow(object):
    def __init__(self, that, image_path):
        self.that = that
        self.path = image_path
        save_screen_shot(that.window_name, self.path, that.get_window_size())
        self.img_file = Image.open(self.path)
        self.last_x = 0
        self.last_y = 0

        self.window = tk.Tk()
        self.window.title("Join")

        img = ImageTk.PhotoImage(self.img_file)

        self.width = img.width() - self.that.frame_width * 2
        self.height = img.height() - self.that.frame_width * 2 - self.that.title_bar_height
        print(self.width, self.height)
        self.window.geometry(str(img.width()) + "x" + str(img.height()))
        self.window.configure(background='grey')

        panel = tk.Label(self.window, image=img)
        panel.bind("<Button-1>", self.clicked)

        panel.pack(side="bottom", fill="both", expand="yes")

        self.window.bind("<Left>", self.left)
        self.window.bind("<Right>", self.right)
        self.window.bind("<Up>", self.up)
        self.window.bind("<Down>", self.down)
        self.window.mainloop()

    def print_fun(self, x, y):
        game_x = x - self.that.frame_width
        game_y = y - self.that.frame_width - self.that.title_bar_height
        print("-----------------------")
        print("Clicked " + str(game_x) + ", " + str(game_y))
        print("Proportions: " + str(game_x / self.width) + ", " + str(game_y / self.height))
        pix = self.img_file.load()
        print("Color: " + str(pix[x, y]))

    def clicked(self, e):
        self.last_x = e.x
        self.last_y = e.y
        self.print_fun(e.x, e.y)

    def left(self, e):
        self.last_x -= 1
        win32api.SetCursorPos((self.last_x + self.window.winfo_rootx(), self.last_y + self.window.winfo_rooty()))
        self.print_fun(self.last_x, self.last_y)

    def right(self, e):
        self.last_x += 1
        win32api.SetCursorPos((self.last_x + self.window.winfo_rootx(), self.last_y + self.window.winfo_rooty()))
        self.print_fun(self.last_x, self.last_y)

    def up(self, e):
        self.last_y -= 1
        win32api.SetCursorPos((self.last_x + self.window.winfo_rootx(), self.last_y + self.window.winfo_rooty()))
        self.print_fun(self.last_x, self.last_y)

    def down(self, e):
        self.last_y += 1
        win32api.SetCursorPos((self.last_x + self.window.winfo_rootx(), self.last_y + self.window.winfo_rooty()))
        self.print_fun(self.last_x, self.last_y)


def save_screen_shot(window_name, file_name, size, start=(0, 0)):
    hwnd = win32gui.FindWindow(None, window_name)
    w_d_c = win32gui.GetWindowDC(hwnd)
    dc_obj = win32ui.CreateDCFromHandle(w_d_c)
    c_d_c = dc_obj.CreateCompatibleDC()
    data_bit_map = win32ui.CreateBitmap()
    data_bit_map.CreateCompatibleBitmap(dc_obj, size[0], size[1])
    c_d_c.SelectObject(data_bit_map)
    c_d_c.BitBlt((0, 0), size, dc_obj, start, win32con.SRCCOPY)
    data_bit_map.SaveBitmapFile(c_d_c, file_name)
    # Free Resources
    dc_obj.DeleteDC()
    c_d_c.DeleteDC()
    win32gui.ReleaseDC(hwnd, w_d_c)
    win32gui.DeleteObject(data_bit_map.GetHandle())


def scan_screen_for_color(colors, size=(800, 600), start=(0, 0)):
    save_screen_shot("Diablo II", "placeholder.bmp", size, start)
    img = Image.open("placeholder.bmp")
    pixels = list(img.getdata())
    width, height = img.size
    pixels = [pixels[i * width:(i + 1) * width] for i in range(height)]
    for i in range(0, len(pixels)):
        for j in range(0, len(pixels[0])):
            if pixels[i][j] in colors:
                return j, i
    return None

if __name__ == '__main__':
    print(save_screen_shot("Diablo II", "placeholder.bmp", (800, 580)))